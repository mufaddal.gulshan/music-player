# music_player

A basic music player written in flutter using iTunes affiliate API.

## Supported devices
Supported on mobiles with Android 4.1 and above

## Supported features
- Ability to search music based on any search term like artist name, track name, etc
- Tap on any search result to immediately start preview track playback
- A mini-player at the bottom to control the playback of the track - skip, play/pause and seek

## Requirements and Instructions to build and deploy the app
- To build the app, you need to have [Dart](https://dart.dev/get-dart) and the [Flutter](https://flutter.dev/docs/get-started/install) SDK installed on your system
- Setup an IDE of your choice. Refer [here](https://flutter.dev/docs/get-started/editor) for detailed instructions.
- Before running the app, make sure to run the `flutter pub get` command
- To test the app, you can run the app directly on an Android simulator using the `flutter run` command
- Build the app using `flutter build apk` command to build a single fat apk supporting all architectures
- Build the app using `flutter build apk --split-per-abi` command to build multiple apks for every device architecture
- Build an app bundle using `flutter build appbundle` command to create an app bundle. While uploading the app bundle, Google Play handles the apk generation from the app bundle
- Detailed instructions can be found [here](https://flutter.dev/docs/deployment/android#building-the-app-for-release)