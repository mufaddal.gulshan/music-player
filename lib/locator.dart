import 'package:get_it/get_it.dart';
import 'package:music_player/services/artist_service.dart';
import 'package:music_player/ui/home_view_model.dart';

GetIt locator = GetIt.instance;

Future<void> setupLocator() async {
  // services
  locator.registerLazySingleton(() => ArtistService());

  // home
  locator.registerFactory(() => HomeViewModel());
}
