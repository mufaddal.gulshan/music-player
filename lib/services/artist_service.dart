import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:music_player/datamodels/result.dart';

class ArtistService {
  String baseUrl = "itunes.apple.com";

  Future<Result> search(String term) async {
    var queryParameters = {'term': term, 'media': 'music'};
    var uri = Uri.https(baseUrl, '/search', queryParameters);
    var response = await http.get(uri);
    if (response.statusCode >= 200 && response.statusCode < 400) {
      return Result.fromJson(jsonDecode(response.body));
    }

    return null;
  }
}
