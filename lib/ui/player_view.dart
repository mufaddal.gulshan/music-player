import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class PlayerView extends StatelessWidget {
  final Function() onNext;
  final Function() onPrev;
  final String url;

  PlayerView({
    @required this.onNext,
    @required this.onPrev,
    @required this.url,
  }) {
    play();
  }

  final AudioPlayer audioPlayer = AudioPlayer(playerId: "1");

  play() {
    audioPlayer.play(url, isLocal: false);
  }

  pause() {
    audioPlayer.pause();
  }

  stop() {
    audioPlayer.stop();
  }

  seek(double value) {
    audioPlayer.seek(Duration(seconds: value.toInt()));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: Icon(Icons.skip_previous, size: 48),
                onPressed: onPrev,
              ),
              StreamBuilder<AudioPlayerState>(
                stream: audioPlayer.onPlayerStateChanged,
                builder: (_, snapshot) {
                  return AnimatedSwitcher(
                    duration: Duration(milliseconds: 200),
                    child: snapshot.data == AudioPlayerState.PLAYING
                        ? IconButton(
                            icon: Icon(Icons.pause, size: 48),
                            onPressed: pause,
                          )
                        : IconButton(
                            icon: Icon(Icons.play_arrow, size: 48),
                            onPressed: play,
                          ),
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.skip_next, size: 48),
                onPressed: onNext,
              ),
            ],
          ),
          SizedBox(height: 16),
          StreamBuilder<Duration>(
            stream: audioPlayer.onDurationChanged,
            builder: (_, duration) => StreamBuilder<Duration>(
              stream: audioPlayer.onAudioPositionChanged,
              builder: (_, position) => Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Slider(
                      value: position.data?.inSeconds?.toDouble() ?? 0,
                      max: duration.data?.inSeconds?.toDouble() ?? 0,
                      onChangeStart: (value) {
                        pause();
                      },
                      onChanged: seek,
                      onChangeEnd: (value) {
                        play();
                      },
                      activeColor: Colors.black,
                      inactiveColor: Colors.black26,
                    ),
                  ),
                  (duration.hasData)
                      ? Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 32.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("${position.data.toHHmm()}"),
                              Text(
                                "${(duration.data - position.data).toHHmm()}",
                              ),
                            ],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}

extension DurationExtension on Duration {
  String toHHmm() {
    return "${this?.inMinutes?.remainder(60)?.toString()?.padLeft(2, "0") ?? "--"}:${this?.inSeconds?.remainder(60)?.toString()?.padLeft(2, "0") ?? "--"}";
  }
}
