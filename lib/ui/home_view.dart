import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:music_player/locator.dart';
import 'package:music_player/ui/player_view.dart';
import 'package:music_player/ui/home_view_model.dart';
import 'package:provider/provider.dart';

const double LIST_TILE_HEIGHT = 88;

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final ScrollController _scrollController = ScrollController();

  AppBar _buildAppBar(HomeViewModel viewModel) {
    return AppBar(
      bottom: PreferredSize(
        child: Container(
          padding: const EdgeInsets.all(16),
          child: TextField(
            decoration: InputDecoration(
              hintText: "Search artist",
              filled: true,
              fillColor: Colors.white,
              border: InputBorder.none,
            ),
            onSubmitted: (term) {
              if (term.isNotEmpty) {
                viewModel.search(term);
              }
            },
          ),
        ),
        preferredSize: Size.fromHeight(kToolbarHeight - 32),
      ),
    );
  }

  Widget _buildBody(HomeViewModel viewModel) {
    if (viewModel.loading) {
      return Center(child: CircularProgressIndicator());
    }

    if (viewModel.songs == null) {
      return Center(child: Text("Type to search artists"));
    }

    if (viewModel.songs.isEmpty) {
      return Center(child: Text("No results found"));
    }

    return ListView.separated(
      controller: _scrollController,
      padding: viewModel.currentIndex == -1
          ? EdgeInsets.zero
          : const EdgeInsets.only(bottom: 200),
      itemBuilder: (context, index) {
        final song = viewModel.songs[index];
        return ListTile(
          leading: Image.network(song.artworkUrl60),
          title: Text(
            song.trackName,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: Text(
            "${song.artistName}\n${song.collectionName}",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          isThreeLine: true,
          selected: index == viewModel.currentIndex,
          trailing: Visibility(
            visible: index == viewModel.currentIndex,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.graphic_eq),
              ],
            ),
          ),
          onTap: () {
            viewModel.currentIndex = index;
          },
        );
      },
      separatorBuilder: (_, __) => Divider(height: 2),
      itemCount: viewModel.songs?.length ?? 0,
    );
  }

  _scroll(HomeViewModel viewModel, double offset) {
    _scrollController.animateTo(
      offset,
      duration: Duration(milliseconds: 200),
      curve: Curves.easeIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomeViewModel>(
      create: (_) => locator<HomeViewModel>(),
      child: Consumer<HomeViewModel>(
        builder: (_, viewModel, __) {
          return Scaffold(
            appBar: _buildAppBar(viewModel),
            body: Stack(
              children: [
                Positioned.fill(child: _buildBody(viewModel)),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: (viewModel.currentIndex != -1)
                      ? ClipRect(
                          child: BackdropFilter(
                            filter:
                                ImageFilter.blur(sigmaX: 20.0, sigmaY: 20.0),
                            child: Container(
                              color: Colors.grey.shade200.withOpacity(0.5),
                              child: PlayerView(
                                onNext: viewModel.currentIndex ==
                                        viewModel.songs.length - 1
                                    ? null
                                    : () {
                                        _scroll(
                                          viewModel,
                                          viewModel.currentIndex *
                                              LIST_TILE_HEIGHT,
                                        );
                                        viewModel.next();
                                      },
                                onPrev: viewModel.currentIndex == 0
                                    ? null
                                    : () {
                                        _scroll(
                                          viewModel,
                                          (viewModel.currentIndex - 1) *
                                              LIST_TILE_HEIGHT,
                                        );
                                        viewModel.previous();
                                      },
                                url: viewModel
                                    .songs[viewModel.currentIndex].previewUrl,
                              ),
                            ),
                          ),
                        )
                      : Container(),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
