import 'package:flutter/material.dart';
import 'package:music_player/datamodels/song.dart';
import 'package:music_player/locator.dart';
import 'package:music_player/services/artist_service.dart';

class HomeViewModel extends ChangeNotifier {
  final _service = locator<ArtistService>();

  bool _loading = false;
  bool get loading => _loading;
  List<Song> _songs;
  List<Song> get songs => _songs;
  int _currentIndex = -1;
  int get currentIndex => _currentIndex;

  set currentIndex(int value) {
    _currentIndex = value;
    notifyListeners();
  }

  void search(String term) async {
    _loading = true;
    notifyListeners();
    var result = await _service.search(term);
    _currentIndex = -1;
    _songs = result.results;
    _loading = false;
    notifyListeners();
  }

  void next() {
    if (_currentIndex < _songs.length - 1) {
      _currentIndex++;
      notifyListeners();
    }
  }

  void previous() {
    if (_currentIndex > 0) {
      _currentIndex--;
      notifyListeners();
    }
  }
}
