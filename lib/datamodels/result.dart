import 'package:music_player/datamodels/song.dart';

class Result {
  int resultCount;
  List<Song> results;

  Result({this.resultCount, this.results});

  Result.fromJson(Map<String, dynamic> json) {
    resultCount = json['resultCount'];
    if (json['results'] != null) {
      results = List<Song>();
      json['results'].forEach((v) {
        results.add(Song.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['resultCount'] = this.resultCount;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
